#import unittest library - needed to run unit tests
import unittest

#import the WeatherInterface class - our test target ('system under test')
from app.app_interface import WeatherInterface

class TestLiveStream(unittest.TestCase):
    def setUp(self):
        #Set up
        self.my_station = WeatherInterface()
        
    def test_report_windspeed (self):
        #Execution
        speed = self.my_station.get_windspeed("kmh")
        #Assertion
        self.assertEqual(speed, "15")

    def test_get_humidity (self):
        #Execution
        humid = self.my_station.get_humidity("kmh", 2)
        #Assertion
        self.assertEqual(humid, "151")