import random

class WeatherInterface:

  def get_windspeed (self, unit):
    # Connect to sensor
    # Get windspeed value
    # convert windspeed value into right unit
    if unit == 'kmh':
        return '15'
    else:
        return '20'

  def get_humidity (self, unit, decimals):
    # Connect to sensor
    # Get humidity value
    # convert humidity value into right unit
    return '151'

  def get_random_number (self):
    return random.randint(0,100)